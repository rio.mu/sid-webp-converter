const gulp = require('gulp')
const cwebp = require('gulp-cwebp')
const del = require('del')
const rename = require('gulp-rename')
const urlSlug = require('url-slug')

const imagePath = {
  src: './input_images/**/*',
  dest: './output_images/',
}

function convertImage() {
  console.log('Converting images...')
  return gulp
    .src(imagePath.src)
    .pipe(cwebp())
    .pipe(gulp.dest(imagePath.dest))
}

function convertImageWithUrlSafe() {
  console.log('Converting images with url safe name...')
  return gulp
    .src(imagePath.src)
    .pipe(cwebp())
    .pipe(
      rename(function (path) {
        return {
          dirname: urlSlug(path.dirname),
          basename: urlSlug(path.basename),
          extname: path.extname,
        }
      })
    )
    .pipe(gulp.dest(imagePath.dest))
}

function removeInputContent() {
  console.log('Removing input images...')
  return del(imagePath.src)
}

function removeOutputContent() {
  console.log('Removing output images...')
  return del('./output_images/*')
}

const convert = gulp.series(removeOutputContent,convertImage)
const slugify = gulp.series(removeOutputContent, convertImageWithUrlSafe)
const clean = gulp.series(removeInputContent,removeOutputContent)

exports.convert = convert
exports.slugify = slugify
exports.clean = clean
exports.default = convert