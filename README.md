# DevTools - Webp Converter using Gulp.js
A tool for converting large numbers of images into webp format also supports renaming image files in a URL safer format.

## Usage
- Place your images under folder `input_images`. You can put image as is or put it in folders under structured directory since the converter support recursive convert. 

## Instalation
```
npm install --global gulp-cli
npm install
```

## Command
- Convert Images: `gulp` or `npm run convert`
- Convert Images & Directory with Safe URL naming: `gulp slugify` or `npm run slugify`
- Remove all images: `gulp clean` or `npm run clean`


## Notes
- Image Quality: **lossless** (Encode the image without any loss) 
- Slugify command not working properly in **more than 3 layers directory**. (the folder structure and folder name will look ugly)

## Credits
- Allah SWT & Rosulullah SAW
- Google & Stackoverflow
- Core enginee using [cwebp](https://developers.google.com/speed/webp/docs/cwebp) and wrapped using [gulp-cwebp](https://www.npmjs.com/package/gulp-cwebp) 